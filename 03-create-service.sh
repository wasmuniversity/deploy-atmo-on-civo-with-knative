#!/bin/bash
cd services
subo create runnable hey #--lang tinygo

: '
update Directive.yaml

  - type: request
    resource: /hey
    method: POST
    steps:
      - fn: hey

* build the bundle:
subo build .

* serve the bundle
subo dev

* test the bundle
http --form POST http://localhost:8080/hello --raw "Bob Morane"
http --form POST http://localhost:8080/hey --raw "Bob Morane"
'
