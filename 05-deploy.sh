#!/bin/bash
export KUBECONFIG=$PWD/config/k3s.yaml
export KUBE_NAMESPACE="demo"

kubectl create namespace ${KUBE_NAMESPACE} --dry-run=client -o yaml | kubectl apply -f -

kn service create atmo-service \
--namespace demo \
--env ATMO_HTTP_PORT="8080" \
--image registry.gitlab.com/wasmuniversity/deploy-atmo-on-civo-with-knative/atmo-services:latest \
--force


: '
* test the bundle
http --form POST http://atmo-service.demo.212.2.246.140.sslip.io/hello --raw "Bob Morane"
http --form POST http://atmo-service.demo.212.2.246.140.sslip.io/hey --raw "Bob Morane"
'
